import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class LecturesController {
    async getLecturesByUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`lecture/user`)
            .bearerToken(accessToken)
            .send();
        return response;
    }

    async saveLecture(accessToken: string, articleObj: object) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`lecture`)
            .bearerToken(accessToken)
            .body(articleObj)
            .send();
        return response;
    }

    async addLecture(accessToken: string, descriptionValue: string, nameValue: string, tagsIdsValue: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('POST')
            .url(`lecture/url`)
            .bearerToken(accessToken)
            .body({
                description: descriptionValue,
                name: nameValue,
                tagsIds: tagsIdsValue,
            })
            .send();
        return response;
    }
}

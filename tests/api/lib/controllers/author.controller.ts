import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class AuthorController {
    async postLogin() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('POST').url(`login`).send();
        return response;
    }

    async getAuthor() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`author/`).send();
        return response;
    }

    async postAuthor() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('POST').url(`author/`).send();
        return response;
    }

    async getUserMe() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`user/me`).send();
        return response;
    }

    async getAuthorOverviewByID(userId: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`author/overview/${userId}`)
            .send();
        return response;
    }
    async postArticle() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('POST').url(`article/`).send();
        return response;
    }

    async getArticleAuthor() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`article/author`).send();
        return response;
    }

    async getAtricleById(atricleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`article/${atricleId}`)
            .send();
        return response;
    }
    async postComment() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('POST').url(`article_comment`).send();
        return response;
    }

    async getAtricleByIdSize(atricleId: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`article_comment/of/${atricleId}?size=200`)
            .send();
        return response;
    }

    async getCourseComment() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('POST').url(`course_comment`).send();
        return response;
    }

    async getRecommendation() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`course/recommended`).send();
        return response;
    }

    async getContinueLearningCourses(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`course/continue/${id}`)
            .send();
        return response;
    }

    async getFavouriteArticles() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`favorite/articles`).send();
        return response;
    }

    async getGoals() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`goals`).send();
        return response;
    }

    async postSubscribe() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`subscription/subscribe`)
            .send();
        return response;
    }
}

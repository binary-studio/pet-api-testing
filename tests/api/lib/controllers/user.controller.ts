import { ApiRequest } from '../request';

let baseUrl: string = global.appConfig.baseUrl;

export class UserController {
    async getCourseComment() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('POST').url(`course_comment`).send();
        return response;
    }

    async getRecommendation() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`course/recommended`).send();
        return response;
    }

    async getContinueLearningCourses(id: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`ourse/continue/${id}`)
            .send();
        return response;
    }

    async getFavouriteArticles() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`favorite/articles`).send();
        return response;
    }

    async getGoals() {
        const response = await new ApiRequest().prefixUrl(baseUrl).method('GET').url(`goals`).send();
        return response;
    }

    async postSubscribe() {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`subscription/subscribe`)
            .send();
        return response;
    }

    async getCurrentUser(accessToken: string) {
        const response = await new ApiRequest()
            .prefixUrl(baseUrl)
            .method('GET')
            .url(`user/me`)
            .bearerToken(accessToken)
            .send();
        return response;
    }
    
}

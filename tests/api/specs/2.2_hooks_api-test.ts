import { expect } from 'chai';
import { checkStatusCode } from '../../helpers/functionsForChecking.helper';
import { AuthController } from '../lib/controllers/auth.controller';
import { LecturesController } from '../lib/controllers/lectures.controller';
import { UserController } from '../lib/controllers/user.controller';
const auth = new AuthController();
const lectures = new LecturesController();
const user = new UserController();

xdescribe('Lecture controller | with hooks', () => {
    let accessToken: string, userId: string;
    let lecturesCounter: number;

    before(`should get access token and userId`, async () => {
        // runs once before the first test in this block
        let response = await auth.authenticateUser('veravong55@gmail.com', 'Password1');
        checkStatusCode(response, 200);
        accessToken = response.body.accessToken;

        response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        userId = response.body.id;
    });

    it(`getLecturesByUser`, async () => {
        let response = await lectures.getLecturesByUser(accessToken);
        checkStatusCode(response, 200);

        lecturesCounter = response.body.length;
    });

    it(`add lecture`, async () => {
        let newLecture = {
            description: 'Very interesting lecture',
            name: 'Test lecture',
            tagsIds: ['c7aa3ce6-4abf-42ba-b0be-dbd4aa46cec4'],
        };

        let response = await lectures.saveLecture(accessToken, newLecture);
        checkStatusCode(response, 200);

        lecturesCounter += 1;
    });

    it(`getLecturesByUser`, async () => {
        let response = await lectures.getLecturesByUser(accessToken);
        checkStatusCode(response, 200);

        expect(response.body.length).to.be.equal(lecturesCounter);
    });

    afterEach(function () {
        // runs after each test in this block
        console.log('It was a test');
    });
});

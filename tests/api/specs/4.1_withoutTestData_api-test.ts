import {
    checkResponseTime,
    checkStatusCode,
    checkResponseBodyStatus,
    checkResponseBodyMessage,
} from '../../helpers/functionsForChecking.helper';
import { LecturesController } from '../lib/controllers/lectures.controller';
const lectures = new LecturesController();

describe('Use test data', () => {
    let invalidLectureDataSet = [
        { description: null, name: 'Test lecture', tagsIds: ['c7aa3ce6-4abf-42ba-b0be-dbd4aa46cec4'] },
        { description: 'Test description', name: null, tagsIds: ['c7aa3ce6-4abf-42ba-b0be-dbd4aa46cec4'] },
        { description: 'Test description', name: 'Test lecture', tagsIds: null },
        { description: '01^', name: 'Test lecture', tagsIds: ['c7aa3ce6-4abf-42ba-b0be-dbd4aa46cec4'] },
        { description: 'Test description', name: '646', tagsIds: ['c7aa3ce6-4abf-42ba-b0be-dbd4aa46cec4'] },
        { description: 'Test description', name: 'Test lecture', tagsIds: 'seaa3ce6-4abf-42ba-b0be-dbd4aa46cec4' },
        { description: 'Test description', name: 'Test lecture', tagsIds: '?>-*()$' },
    ];

    invalidLectureDataSet.forEach((credentials) => {
        it('create lecture using invalid credentials : ${credentials.description} + ${credentials.name} +${credentials.tagsIds}', async () => {
            let response = await lectures.addLecture(
                credentials.description,
                credentials.name,
                credentials.tagsIds
            );

            checkStatusCode(response, 400);
            checkResponseBodyStatus(response, 'Bad Request');
            checkResponseBodyMessage(response, 'Please fill in all required fields');
            checkResponseTime(response, 3000);
        });
    });
});

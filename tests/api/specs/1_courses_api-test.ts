import { expect } from 'chai';
import { checkResponseTime, checkStatusCode } from '../../helpers/functionsForChecking.helper';
import { CoursesController } from '../lib/controllers/courses.controller';
import { UserController } from '../lib/controllers/user.controller';
import { AuthorController } from '../lib/controllers/author.controller';
import { AuthController } from '../lib/controllers/auth.controller';
const courses = new CoursesController();
const user = new UserController();
const author = new AuthorController();
const auth = new AuthController();

const schemas = require('./data/schemas_testData.json');
var chai = require('chai');
chai.use(require('chai-json-schema'));

describe('Courses controller', () => {
    let userId: string;
    let atricleId: string;
    let id: string;
    let accessToken: string;

    it(`login`, async () => {
        let response = await auth.authenticateUser('veravong55@gmail.com', 'Password1');
        checkStatusCode(response, 200);
        accessToken = response.body.accessToken;
    });

    it(`get current user details`, async () => {
        let response = await user.getCurrentUser(accessToken);
        checkStatusCode(response, 200);
        userId = response.body.id;
    });

    it(`postLogin`, async () => {
        let response = await author.postLogin();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
    });

    it(`getAuthor`, async () => {
        let response = await author.getAuthor();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getAuthor);
    });

    it(`postAuthor`, async () => {
        let response = await author.postAuthor();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postAuthor);
    });

    it(`getUserMe`, async () => {
        let response = await author.getUserMe();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getUserMe);
    });

    it(`getAuthorOverviewByID`, async () => {
        let response = await author.getAuthorOverviewByID(userId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);

        userId = response.body[0].id;
    });

    it(`postArticle`, async () => {
        let response = await author.postArticle();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postArticle);
    });

    it(`getArticleAuthor`, async () => {
        let response = await author.getArticleAuthor();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getArticleAuthor);
    });

    it(`getAtricleById`, async () => {
        let response = await author.getAtricleById(atricleId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getAtricleById);

        atricleId = response.body[6].id;
    });

    it(`postComment`, async () => {
        let response = await author.postComment();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postComment);
    });

    it(`getAtricleByIdSize`, async () => {
        let response = await author.getAtricleByIdSize(atricleId);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getAtricleByIdSize);

        atricleId = response.body[6].id;
    });

    it(`getCourseComment`, async () => {
        let response = await user.getCourseComment();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getCourseComment);
    });

    it(`getRecommendation`, async () => {
        let response = await user.getRecommendation();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getRecommendation);
    });

    it(`getContinueLearningCourses`, async () => {
        let response = await user.getContinueLearningCourses(id);

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getContinueLearningCourses);

        id = response.body[0].id;
    });

    it(`getFavouriteArticles`, async () => {
        let response = await user.getFavouriteArticles();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getFavouriteArticles);
    });

    it(`getGoals`, async () => {
        let response = await user.getGoals();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_getGoals);
    });

    it(`postSubscribe`, async () => {
        let response = await user.postSubscribe();

        checkStatusCode(response, 200);
        checkResponseTime(response, 3000);
        expect(response.body).to.be.jsonSchema(schemas.schema_postSubscribe);
    });
});
